package com.example.calculator

import android.icu.lang.UCharacter.GraphemeClusterBreak.V
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.ButtonBarLayout
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.Button as Button

class MainActivity : AppCompatActivity(),View.OnClickListener {
    private var firstvariable = 0.0
    private var secondvariable = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        deletebutton.setOnLongClickListener(){
            resulttext.text = ""
            operation.isEmpty()
            true
        }
        wertilibutton.setOnClickListener {
            if(resulttext.text.isNotEmpty() && "." !in resulttext.text.toString()){
                resulttext.text = resulttext.text.toString() + "."
            }
        }



    }
    fun equal(view: View){
        val value = resulttext.text.toString()
        if(value.isNotEmpty()) {
            secondvariable = value.toDouble()
            var result: Double = 0.0
            if(operation == ":" && secondvariable.toDouble() == 0.0){
                Toast.makeText(this, "Cannot Divide By Zero", Toast.LENGTH_SHORT).show()
            }
            if (operation == ":" && secondvariable.toDouble() != 0.0) {
                result = firstvariable / secondvariable


            }
            resulttext.text = result.toString()
            if(operation == "+"){
                result = firstvariable + secondvariable
            }
            resulttext.text = result.toString()
            if(operation == "-"){
                result = firstvariable - secondvariable
            }
            resulttext.text = result.toString()
            if(operation == "x"){
                result = firstvariable * secondvariable
            }
            resulttext.text = result.toString()

        }



    }
    fun plus(view: View){
        val value = resulttext.text.toString()
        if(value.isNotEmpty()){
            operation = "+"
            firstvariable = value.toDouble()
            resulttext.text =""
        }
    }
    fun multiply(view: View){
        val value = resulttext.text.toString()
        if(value.isNotEmpty()){
            operation = "x"
            firstvariable = value.toDouble()
            resulttext.text = ""
        }

    }
    fun minus(view: View){
        val value = resulttext.text.toString()
        if(value.isNotEmpty()){
            operation = "-"
            firstvariable = value.toDouble()
            resulttext.text = ""
        }
    }
    fun divide(view: View){
        val value = resulttext.text.toString()
        if(value.isNotEmpty()) {
            operation = ":"
            firstvariable = value.toDouble()
            resulttext.text = ""
        }

    }

    fun delete(view: View){
        val value = resulttext.text.toString()
        if(value.isNotEmpty()) {
            resulttext.text = value.substring(0,value.length-1)
        }


    }

    override fun onClick(v: View?) {
       val button = v as Button
        resulttext.text = resulttext.text.toString() + button.text.toString()

    }


}